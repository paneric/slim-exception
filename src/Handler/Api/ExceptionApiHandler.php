<?php

declare(strict_types=1);

namespace Paneric\Slim\Exception\Handler\Api;

use JsonException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Response;
use Throwable;

abstract class ExceptionApiHandler
{
    public function __construct(protected LoggerInterface $errorLogger)
    {
    }

    /**
     * @throws JsonException
     */
    public function __invoke(
        ServerRequestInterface $request,
        Throwable $e,
        bool $displayErrorDetails
    ): Response {
        $this->errorLogger->error(sprintf(
            "%s%s%s%s",
            $e->getFile() . "\n",
            $e->getLine() . "\n",
            $e->getMessage() . "\n",
            $e->getTraceAsString() . "\n"
        ));

        $data = [
            'status' => $e->getCode(),
            'error' => $e->getMessage()
        ];

        $response = new Response();
        $response = $response->withStatus($e->getCode());
        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');
        $response->getBody()
            ->write(json_encode(
                $data,
                JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            ));

        return $response;
    }
}
