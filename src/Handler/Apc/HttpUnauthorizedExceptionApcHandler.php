<?php

declare(strict_types=1);

namespace Paneric\Slim\Exception\Handler\Apc;

class HttpUnauthorizedExceptionApcHandler extends ExceptionApcHandler
{
}
