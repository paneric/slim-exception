<?php

declare(strict_types=1);

namespace Paneric\Slim\Exception\Handler\Apc;

class HttpNotFoundExceptionApcHandler extends ExceptionApcHandler
{
}
