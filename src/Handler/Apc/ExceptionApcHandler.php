<?php

declare(strict_types=1);

namespace Paneric\Slim\Exception\Handler\Apc;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Response;
use Throwable;

abstract class ExceptionApcHandler
{
    public function __construct(protected LoggerInterface $errorLogger)
    {
    }

    public function __invoke(
        ServerRequestInterface $request,
        Throwable $e,
        bool $displayErrorDetails
    ): Response {
        $this->errorLogger->error(sprintf(
            "%s%s%s%s",
            $e->getFile() . "\n",
            $e->getLine() . "\n",
            $e->getMessage() . "\n",
            $e->getTraceAsString() . "\n"
        ));

        $response = new Response();
        $response = $response->withStatus($e->getCode());
        $response = $response->withHeader('Content-Type', 'text/html; charset=utf-8');
        $response->getBody()->write($e->getMessage());

        return $response;
    }
}
